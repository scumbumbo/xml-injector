# XML Injector version 2
# by Scumbumbo @ MTS
#
# Functions in the add_to_tuning module are called by the snippet's _tuning_loaded_callback
# in order to process the addition of affordances to the various game objects.
#
# This mod is intended as a standard for modder's to use as a shared library.  Please do not
# distribute any modifications anywhere other than the mod's main download site.  Modification
# suggestions and bug notices should be communicated to the maintainer, currently Scumbumbo at
# the Mod The Sims website - http://modthesims.info/member.php?u=7401825
#

import services
import sims4.log
from objects.definition_manager import DefinitionManager

logger = sims4.log.Logger('XmlInjector')

import inspect

OBJECT_SIM = 14965  # The instance ID for the object_sim tuning
TESTING = False     # If testing then allow adding multiple copies of affordance to _super_affordances

def add_super_affordances_to_objects(object_selection, sa_list):
    for tun in object_selection.get_objects():
        if hasattr(tun, '_super_affordances'):
            sa_to_add_list = []
            for sa in sa_list:
                if not sa in tun._super_affordances or TESTING:
                    sa_to_add_list.append(sa)
            if len(sa_to_add_list) > 0:
                logger.info('  {}: adding super_affordances: {}', tun, sa_to_add_list)
                tun._super_affordances += tuple(sa_to_add_list)

def add_super_affordances_to_sims(sa_list):
    definition_manager = services.definition_manager()
    object_sim = super(DefinitionManager, definition_manager).get(OBJECT_SIM)
    sa_to_add_list = []
    for sa in sa_list:
        if not sa in object_sim._super_affordances or TESTING:
            sa_to_add_list.append(sa)
    if len(sa_to_add_list) > 0:
        logger.info('  {}: adding super_affordances: {}', object_sim, sa_to_add_list)
        object_sim._super_affordances += tuple(sa_to_add_list)

def add_super_affordances_to_phones(sa_list):
    definition_manager = services.definition_manager()
    object_sim = super(DefinitionManager, definition_manager).get(OBJECT_SIM)
    sa_to_add_list = []
    for sa in sa_list:
        if not sa in object_sim._phone_affordances or TESTING:
            sa_to_add_list.append(sa)
    if len(sa_to_add_list) > 0:
        logger.info('  phones: adding super_affordances: {}', sa_to_add_list)
        object_sim._phone_affordances += tuple(sa_to_add_list)

def add_super_affordances_to_relpanel(sa_list):
    definition_manager = services.definition_manager()
    object_sim = super(DefinitionManager, definition_manager).get(OBJECT_SIM)
    sa_to_add_list = []
    for sa in sa_list:
        if not sa in object_sim._relation_panel_affordances or TESTING:
            sa_to_add_list.append(sa)
    if len(sa_to_add_list) > 0:
        logger.info('  relpanel: adding super_affordances: {}', sa_to_add_list)
        object_sim._relation_panel_affordances += tuple(sa_to_add_list)

def add_mixer_to_affordance_list(affordance_lists_list, mixer_list):
    for affordance_list in affordance_lists_list:
        mixers_to_add_list = []
        for mixer in mixer_list:
            if not mixer in affordance_list.value or TESTING:
                mixers_to_add_list.append(mixer)
        if len(mixers_to_add_list) > 0:
            logger.info('  {}: adding mixer interactions: {}', affordance_list, mixers_to_add_list)
            affordance_list.value += tuple(mixers_to_add_list)

def add_to_loot_actions(loot_actions, loot_action_variant_list):
    logger.info('  {}: adding loot actions: {}', loot_actions, loot_action_variant_list)
    saved_loot_actions = loot_actions.loot_actions
    loot_actions.loot_actions += loot_action_variant_list
    try:
        loot_actions._validate_recursion()
    except RecursionError:
        logger.error(' Added loot actions create a recursion, this would throw exceptions when used in game.')
        logger.error(' Loot action changes reverted')
        loot_actions.loot_actions = saved_loot_actions

def add_to_random_loot_actions(random_loot_actions, random_loot_actions_list):
    logger.info('  {}: adding random loot actions: {}', random_loot_actions, random_loot_actions_list)
    saved_loot_actions = random_loot_actions.random_loot_actions
    random_loot_actions.random_loot_actions += random_loot_actions_list
    try:
        random_loot_actions._validate_recursion()
    except RecursionError:
        logger.error(' Added random loot actions create a recursion, this would throw exceptions when used in game.')
        logger.error(' Random loot action changes reverted')
        random_loot_actions.random_loot_actions = saved_loot_actions

def add_states_to_objects(object_selection, new_state_component):
    for tun in object_selection.get_objects():
        if hasattr(tun, '_components') and hasattr(tun._components, 'state'):
            state_component = tun._components.state
            if new_state_component.states:
                logger.info('  {}: adding states: {}', tun, new_state_component.states)
                state_component._tuned_values = state_component._tuned_values.clone_with_overrides(states=state_component._tuned_values.states + new_state_component.states)
            if new_state_component.state_triggers:
                logger.info('  {}: adding state_triggers: {}', tun, new_state_component.state_triggers)
                state_component._tuned_values = state_component._tuned_values.clone_with_overrides(state_triggers=state_component._tuned_values.state_triggers + new_state_component.state_triggers)
                
def add_name_component_to_objects(object_selection, name_component):
    for tun in object_selection.get_objects():
        if hasattr(tun, '_components') and hasattr(tun._components, 'name'):
            if tun._components.name is None:
                logger.info('  {}: adding name component: {}', tun, name_component._tuned_values)
                tun._components = tun._components.clone_with_overrides(name=name_component)
            else:
                logger.error(' {}: already has name component, cannot add', tun)

def add_object_relationships_to_objects(object_selection, object_relationships_component):
    for tun in object_selection.get_objects():
        if hasattr(tun, '_components') and hasattr(tun._components, 'object_relationships'):
            if tun._components.object_relationships is None:
                logger.info('  {}: adding object_relationships component: {}', tun, object_relationships_component._tuned_values)
                tun._components = tun._components.clone_with_overrides(object_relationships=object_relationships_component)
            else:
                logger.error(' {}: already has object_relationships component, cannot add', tun)

def add_buffs_to_trait(trait, buffs_list):
    logger.info('  {}: adding buffs: {}', trait, [b.buff_type for b in buffs_list])
    trait.buffs += buffs_list
