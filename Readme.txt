This is release candidate 1 of version 2 of the XML Injector

*** DO NOT USE FOR PRODUCTION MODS ***

This release candidate is for testing the final set of new features added to version 2
of XML Injector and for preparing mods that will require these new features.

Please direct all comments regarding any issues encountered in testing, questions
and ideas on any changes to the #xml_injector-tech channel on the Scumbumbo Werld
Discord server.

https://discord.gg/xV9VYuw

Barring any additional show-stopper ideas, these are the final new features that will
be added to version 2.  These are the current working changes:

XML INJECTOR DETECTION
----------------------

A standardized drama node has been created which, when included in a package, will trigger
a notification if XML Injector version 2+ is not installed.

Modders are encouraged to edit the string table entry to specify additional information
such as their mod name, and where to find their own documentation for how to get and
install the XML Injector.

Modders should NOT change the actual XML for the drama node.  This drama node will be
disabled by the XML Injector (version 2+) so that the alert no longer appears if the
injector is properly installed and working.

Special thanks to Triplis, Andrew (Sims 4 Studio) and the others on my Discord who helped
to figure out this easy to use detection and alert method which, true to the design of the
mod, requires no scripting at all on the modder's part.

VERSION CHECKING OPTION
-----------------------

A version check can now be included in snippets which specifies the minimum version
of the XML Injector which supports your snippet.  Players with version 1 of the mod
will not receive any alerts as this check is not in the original version.  However,
in the future this will alert players who download a mod of the requirement to
update their copy of the injector.

A generic alert dialog will be presented to the player when their lot is loaded into
the game notifying them that they have a mod which requires a newer version.  A custom
dialog may optionally be added to your snippet identifying any specific information
you may feel is important apart from the generic text (e.g. translated text).  Only a
single dialog will be displayed if more than one installed mod requires an updated
version.

The generic alert is as follows:

Dialog title:  XML Injector Version
Dialog text:   One of your installed mods requires a newer version of the XML Injector.
               That mod will likely not function properly until you upgrade your copy
               of XML Injector to the latest version.

TAGGED OBJECT SELECTION
-----------------------

The object_selection variant for add_interactions_to_objects can now test for COBJ
tags, e.g. Func_Fridge, and all objects in the game with the chosen tag will have their
_super_affordances modified.

ADD LOOT ACTIONS
----------------

Additional loot actions can be added to LootActions and RandomWeighedLoot tunings (e.g.
loot_Buffs).  These new loot actions can include any required tests or weights in the
same manner as the normal syntax these would normally be specified in the XML.

If an "actions" tuning is included in a LootActions or RandomWeightedLoot, this can cause
recursion.  The game typically tests for this when the problem is run into during game play
and throws an exception.  However, after adding to loot actions using this snippet, recursion
is tested immediately and if a loot action calls itself in a loop the snippet will be rejected
without throwing an exception - instead the error will be logged using the normal game log
file methods (requires Sims Log Enabler).

ADD BUFFS TO TRAITS
-------------------

Allows additional buffs to be added to the buffs list of Traits.  The new buffs can include
all the standard TunableBuffReference entries, buff_reason and buff_type.

ADD NAME COMPONENT TO OBJECTS
-----------------------------

Allows a name component to be added to objects of the game.  Uses the same object_selection
methods as add_interactions_to_objects.  All of the standard name component tunings can be
included: affordance, allow_description, allow_name and templates.

ADD OBJECT RELATIONSHIP COMPONENT TO OBJECTS
--------------------------------------------

This will allow adding an object relationship component to the objects in the game.  Uses the
same object_selection methods as add_interactions_to_objects.  MOST of the standard object
relationship tunings can be added via this with the exception of the icon_override tuning.
An icon_override requires modification of the SimData resource for the object which cannot
be done via Python scripting.  Because of this, if an icon_override behavior is required by
your mod you are better off changing the object tuning by overriding the original EA object
tuning.

ADD STATES TO OBJECTS
---------------------

This allows adding state_triggers and states to the state component of game objects.  All of
the standard state_trigger and state tunings are supported.

EXAMPLES
--------

<?xml version="1.0" encoding="utf-8"?>
<I c="XmlInjector" i="snippet" m="xml_injector.snippet" n="Scumbumbo_XmlInjector_Test3" s="10608251930441092051">
  <!--                          -->
  <!-- ************************ -->
  <!-- * VERSION TEST EXAMPLE * -->
  <!-- ************************ -->
  <!--                                                                                      -->
  <!-- This example will trigger an alert as it claims to require version 3 of XML Injector -->
  <!-- Note that this will not display properly as is without a string table being setup!   -->
  <!--                                                                                      -->
  <T n="xml_injector_minimum_version">3</T>
  <V n="version_error_dialog" t="enabled">
    <U n="enabled">
      <V n="text" t="single">
        <T n="single">0xA3D96FDD<!--String: "<b><i>Custom Mod Name</i></b> requires version 3 of the XML Injector library.\n\n<font size='14'>The mod will not operate properly until you have updated your copy of the XML Injector to the latest version.</font>"--></T>
      </V>
      <V n="title" t="enabled">
        <T n="enabled">0xD24B65B9<!--String: "Version Error"--></T>
      </V>
    </U>
  </V>
  <!--                             -->
  <!-- *************************** -->
  <!-- * TAGGED OBJECT SELECTION * -->
  <!-- *************************** -->
  <!--                                                                                   -->
  <!-- Note that this requires the standard XML Injector test package to be installed to -->
  <!-- find the required interaction to be added to the refrigerators.                   -->
  <!--                                                                                   -->
  <L n="add_interactions_to_objects">
    <U>
      <V n="object_selection" t="objects_with_tag">
        <U n="objects_with_tag">
          <E n="tag">Func_Fridge</E>
        </U>
      </V>
      <L n="_super_affordances">
        <T>12415634905840155217<!--ImmediateSuperInteraction: Scumbumbo_XmlInjector_InteractionTest--></T>
      </L>
    </U>
  </L>
  <!--                        -->
  <!-- ********************** -->
  <!-- * ADD TO LootActions * -->
  <!-- ********************** -->
  <!--                                                                             -->
  <!-- This will add a money loot which will occur when the LootActions            -->
  <!-- loot_Buff_Sink_BrushTeeth is triggered (any time a sim brushes their teeth. -->
  <!--                                                                             -->
  <L n="add_to_loot_actions">
    <U>
      <T n="loot_actions_ref">24459<!--LootActions: loot_Buff_Sink_BrushTeeth--></T>
      <L n="loot_actions_to_add">
        <V t="money_loot">
          <U n="money_loot">
            <V n="amount" t="random_in_range">
              <U n="random_in_range">
                <T n="lower_bound">75</T>
                <T n="upper_bound">200</T>
              </U>
            </V>
            <E n="subject">Actor</E>
          </U>
        </V>
      </L>
    </U>
  </L>
  <!--                               -->
  <!-- ***************************** -->
  <!-- * ADD TO RandomWeightedLoot * -->
  <!-- ***************************** -->
  <!--                                                                                   -->
  <!-- This adds a loot action to create a trash pile instead of usable seeds when the   -->
  <!-- randomWeightedLoot_Garden_SeedPacket_StarterFlower is triggered (a sim has opened -->
  <!-- a starter flower seeds packet).  If the sim has the green thumb trait then this   -->
  <!-- will never occur (weight multiplier of zero).                                     -->
  <!--                                                                                   -->
  <L n="add_to_random_loot_actions">
    <U>
      <T n="random_weighted_loot_ref">188636<!--RandomWeightedLoot: randomWeightedLoot_Garden_SeedPacket_StarterFlower--></T>
      <L n="random_loot_actions_to_add">
        <U>
          <V n="action" t="create_object">
            <U n="create_object">
              <V n="creation_data" t="definition">
                <U n="definition">
                  <T n="definition">22174</T>
                </U>
              </V>
              <V n="location" t="position">
                <U n="position">
                  <T n="allow_off_lot_placement">True</T>
                </U>
              </V>
            </U>
          </V>
          <U n="weight">
            <T n="base_value">1</T>
            <L n="multipliers">
              <U>
                <T n="multiplier">0</T>
                <L n="tests">
                  <L>
                    <V t="trait">
                      <U n="trait">
                        <E n="subject">Actor</E>
                        <L n="whitelist_traits">
                          <T>35511<!--Trait: trait_SuperGreenThumb--></T>
                        </L>
                      </U>
                    </V>
                  </L>
                </L>
              </U>
            </L>
          </U>
        </U>
      </L>
    </U>
  </L>
  <!--                        -->
  <!-- ********************** -->
  <!-- * ADD BUFFS TO TRAIT * -->
  <!-- ********************** -->
  <!--                                                                                  -->
  <!-- This elegantly stupid example adds a buff to all human Sims which indicates that -->
  <!-- they are experiencing sadness from the death of a friend.  The actual reason for -->
  <!-- this will display on the buff as having been from playing cards.  A second buff  -->
  <!-- is added to make the Sim a continuously embarrassed loner (with no reason        -->
  <!-- specified).  I keep forgetting to take this stupid example out of my game and    -->
  <!-- now all my Sims are permanently depressed.  Joy!                                 -->
  <!--                                                                                  -->
  <L n="add_buffs_to_trait">
    <U>
      <T n="trait">151039<!--Trait: trait_Species_Human--></T>
      <L n="buffs">
        <U>
          <V n="buff_reason" t="enabled">
            <T n="enabled">0x4CD798B0<!--String: "(From Playing Cards)"--></T>
          </V>
          <T n="buff_type">12757<!--Buff: Buff_WitnessedDeathFriend--></T>
        </U>
        <U>
          <T n="buff_type">29532<!--Buff: Buff_Embarrassed_Loner--></T>
        </U>
        <U>
          <T n="buff_type">29532<!--Buff: Buff_Embarrassed_Loner--></T>
        </U>
      </L>
    </U>
  </L>
  <!--                                          -->
  <!-- **************************************** -->
  <!-- * ADD AN OBJECT RELATIONSHIP COMPONENT * -->
  <!-- **************************************** -->
  <!--                                                                               -->
  <!-- This example adds an object relationship to any objects with the generic_cook -->
  <!-- interaction on them (stoves, counters, etc.)  You will likely not want to     -->
  <!-- repurpose the stuffed animal relationship statistic for your own work, but it -->
  <!-- does function.                                                                -->
  <!--                                                                               -->
  <!-- Note that this particular example just sets up the relationship component to  -->
  <!-- track the relationship, but additional changes to interactions or loots are   -->
  <!-- required for sims to actual increase or decrease their relationship score     -->
  <!-- with an object.                                                               -->
  <!--                                                                               -->
  <L n="add_object_relationships_to_objects">
    <U>
      <V n="object_selection" t="objects_with_affordance">
        <U n="objects_with_affordance">
          <T n="affordance">13434<!--SuperInteraction: generic_cook--></T>
        </U>
      </V>
      <U n="object_relationships_component">
        <T n="relationship_stat">10229<!--Statistic: statistic_Object_StuffedAnimalRelationship--></T>
        <V n="relationship_track_visual" t="enabled">
          <U n="enabled">
            <T n="relationship_track">16650<!--RelationshipTrack: LTR_Friendship_Main--></T>
          </U>
        </V>
      </U>
    </U>
  </L>
  <!--                          -->
  <!-- ************************ -->
  <!-- * ADD A NAME COMPONENT * -->
  <!-- ************************ -->
  <!--                                                                         -->
  <!-- This example adds a name component to any objects with the generic_cook -->
  <!-- interaction on them, allowing the player to set a name on their stoves  -->
  <!-- and counter tops.                                                       -->
  <!--                                                                         -->
  <!-- I don't remember what string I put in there for the template_name, I    -->
  <!-- think it's "Packing Crate" from that mod.  This is just an example,     -->
  <!-- you'll be wanting to use your own string anyway.                        
  <!--                                                                         -->
  <L n="add_name_component_to_objects">
    <U>
      <V n="object_selection" t="objects_with_affordance">
        <U n="objects_with_affordance">
          <T n="affordance">13434<!--SuperInteraction: generic_cook--></T>
        </U>
      </V>
      <U n="name_component">
        <L n="templates">
          <U>
            <T n="template_name">0x51EE27F1</T>
          </U>
        </L>
      </U>
    </U>
  </L>
  <!--                -->
  <!-- ************** -->
  <!-- * ADD STATES * -->
  <!-- ************** -->
  <!--                                                                              -->
  <!-- This is more of a real world example than the others.  This example adds the -->
  <!-- client states required to be added to stoves and counters by my Don't Prep   -->
  <!-- Food Where You Angry Poop.  This, combined with using XML Injector to add    -->
  <!-- the required interactions, will serve to completely eliminate the script     -->
  <!-- currently required by that mod!                                              -->
  <!--                                                                              -->
  <!-- Note that the state_triggers list of an object's state component may also be -->
  <!-- be modified in the same way, but this is not demonstrated here.  See the     -->
  <!-- tuning description if you are interested in that, it works just like the     -->
  <!-- "real thing".                                                                -->
  <!--                                                                              -->
  <L n="add_states_to_objects">
    <U>
      <V n="object_selection" t="objects_with_affordance">
        <U n="objects_with_affordance">
          <T n="affordance">13434<!--SuperInteraction: generic_cook--></T>
        </U>
      </V>
      <U n="state_component">
        <L n="states">
          <U>
            <L n="client_states">
              <U>
                <T n="key">11715010438114361249<!--ObjectStateValue: MTS_Scumbumbo_Counter_EnableStaging--></T>
              </U>
              <U>
                <T n="key">15102775808069486920<!--ObjectStateValue: MTS_Scumbumbo_Counter_DisableStaging--></T>
              </U>
            </L>
            <V n="default_value" t="reference">
              <T n="reference">15102775808069486920<!--ObjectStateValue: MTS_Scumbumbo_Counter_DisableStaging--></T>
            </V>
          </U>
        </L>
      </U>
    </U>
  </L>
</I>